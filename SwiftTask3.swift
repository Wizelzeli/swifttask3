// Написать сортировку массива с помощью замыкания, в одну сторону, затем в обратную

var array = [124, 2, 23, 664, 23, 72, 135, 12]
let arraySorted1 = array.sorted(by: <) 
let arraySorted2 = array.sorted(by: >)

// Вывести результат в консоль

print("\nСортировка по возрастанию: \(arraySorted1)")
print("Сортировка по убыванию: \(arraySorted2)")

// Создать метод, который принимает имена друзей, после этого имена положить в массив

var friendsNames = [String]()

func putNamesInArray(name: String) {
	friendsNames.append(name) 
}

putNamesInArray(name: "Egor") 
putNamesInArray(name: "Alexandr") 
putNamesInArray(name: "Dmitriy")

// Массив отсортировать по количеству букв в имени

print(friendsNames.sorted(by: {$0.count < $1.count}))

// Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга

var dictionary = [String : Int]()

// Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение

func putInDictionary(key: String, value: Int) {
	print("Ключ: \(key), значение: \(value)")
	dictionary[key] = value
}

putInDictionary(key:"Egor", value: 4)
putInDictionary(key:"Alexandr", value: 8)
putInDictionary(key:"Dmitriy", value: 7)

for (key, value) in dictionary { 
		print("Ключ: \(key), значение: \(value)")
}

// Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль

var arrStrings = [String]() 
var arrInts = [Int]()

func emptyArraysCheck(ints: [Int], strings: [String]) {
	if arrStrings.isEmpty && arrInts.isEmpty {
		arrStrings.append("empty")
		arrInts.append(0)
	} else if arrStrings.isEmpty { 
		arrStrings.append("empty") 
	} else if arrInts.isEmpty { 
		arrInts.append(0)
	} 
	print(arrStrings) 
	print(arrInts)
}

arrStrings.append("test1")
arrStrings.append("test2")
arrStrings.append("test3")

emptyArraysCheck(ints: arrInts, strings: arrStrings)